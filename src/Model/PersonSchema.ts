export class PersonSchema
{
    public PersonId: number;
    public FirstName: string;
    public LastName: string;
    public Age: number;
    public BirthPlace: string;
}