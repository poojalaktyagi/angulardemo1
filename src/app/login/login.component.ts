import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms'
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  apiUrl = "http://localhost:8080/userlogin";
  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  })
  constructor(private http: HttpClient, private cookieService: CookieService, private router: Router) {
    let jwtoken = this.cookieService.get('jwtoken');
    if (jwtoken !== undefined && jwtoken !== "") {
      window.location.href = ('/dashboard');
    }
  }
  ngOnInit() {
  }
  onSubmit() {
    let ApiLoginURL = this.apiUrl + "?username=" + this.loginForm.value.username + "&password=" + this.loginForm.value.password;

    this.http.post(ApiLoginURL, null).subscribe((dataFromApi) => {
      if (dataFromApi === undefined || (dataFromApi !== undefined && dataFromApi["Authenticated"] == false)) {
        console.warn("Invalid Login");
        return;
      }
      this.cookieService.set('jwtoken', dataFromApi["data"].token);
      window.location.href = ('/dashboard');
    })
  }
}
