import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonDataModifierModalComponent } from './person-data-modifier-modal.component';


describe('PersonDataModifierModalComponent', () => {
  let component: PersonDataModifierModalComponent;
  let fixture: ComponentFixture<PersonDataModifierModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonDataModifierModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonDataModifierModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
