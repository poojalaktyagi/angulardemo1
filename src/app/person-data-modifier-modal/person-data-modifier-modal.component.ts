import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { PersonDashboardComponent } from '../person-dashboard/person-dashboard.component';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { PersonSchema } from 'src/Model/PersonSchema';
import { ErrorSchema } from 'src/Model/ErrorSchema';
@Component({

  selector: 'app-person-data-modifier-modal',
  templateUrl: './person-data-modifier-modal.component.html',
  styleUrls: ['./person-data-modifier-modal.component.css']
})
export class PersonDataModifierModalComponent implements OnInit {
  @Input() ErrorSchemaObj: ErrorSchema = new ErrorSchema();
  personemodifiermodalform: FormGroup;
  @Input() CrrntPerson: PersonSchema = new PersonSchema();
  @Input() PersonDashboard: PersonDashboardComponent;
  @ViewChild('PersonModifierModalTrigger') personModifierModalTrigger;
  @ViewChild('CloseModal') private closeModal;

  constructor(private http: HttpClient, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.personemodifiermodalform = new FormGroup({
      firstName: new FormControl(this.CrrntPerson.FirstName, Validators.required),
      lastName: new FormControl(this.CrrntPerson.LastName),
      age: new FormControl(this.CrrntPerson.Age),
      birthplace: new FormControl(this.CrrntPerson.BirthPlace)
    });
  }

  bindCrrntPerson(crrntPersonData) {
    if (crrntPersonData == null) {
      this.clearCrrntPerson();
      return;
    }

    this.CrrntPerson.PersonId = crrntPersonData.PersonId;
    this.CrrntPerson.FirstName = crrntPersonData.FirstName;
    this.CrrntPerson.LastName = crrntPersonData.LastName;
    this.CrrntPerson.Age = crrntPersonData.Age;
    this.CrrntPerson.BirthPlace = crrntPersonData.BirthPlace;
  }
  async EditPerson() {
    let APIBaseURL = "/api/";
    if (this.CrrntPerson != null && this.CrrntPerson.PersonId > 0) {
      //personid=1&firstname=Pooja&lastname=Lalit tyagi&birthplace=Merrut&age=28
      APIBaseURL += "updateperson?personid=" + this.CrrntPerson.PersonId + "&";
    }
    else {
      // firstname=kajal&lastname=tyagi&age=21&birthplace=delhi
      APIBaseURL += "insertperson?";
    }

    APIBaseURL += "firstname=" + this.personemodifiermodalform.value.firstName + "&lastname=" + this.personemodifiermodalform.value.lastName + "&age=" + this.personemodifiermodalform.value.age + "&birthplace=" + this.personemodifiermodalform.value.birthplace; // get it from html form inputs

    await this.http.post(APIBaseURL, { withCredentials: true }).subscribe((dataFromApi) => {
      if (typeof dataFromApi === "string" && (dataFromApi.indexOf("error:") >= 0 || dataFromApi == "jwt must be provided")) {
        console.warn(dataFromApi);
        this.ErrorSchemaObj.ErrorMessage = "Error Found";
      }
      else {
        if (this.PersonDashboard !== undefined && this.PersonDashboard != null)
          this.PersonDashboard.updateUI();

        this.clearCrrntPerson();
        this.closeModal.nativeElement.click();
      }
    })
  }

  clearCrrntPerson() {
    this.CrrntPerson.PersonId = 0;
    this.CrrntPerson.FirstName = "";
    this.CrrntPerson.LastName = "";
    this.CrrntPerson.Age = 0;
    this.CrrntPerson.BirthPlace = "";
  }
}
