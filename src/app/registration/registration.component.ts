import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { FormGroup, FormControl } from '@angular/forms'

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  title = "My App";
  apiUrl = "http://localhost:8080/registration";
  registraionForm = new FormGroup({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    username: new FormControl(''),
    password: new FormControl('')
  })

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }
  onSubmit() {
    console.warn(this.registraionForm.value);
    let ApiRegistrationURL = this.apiUrl + "?firstname=" + this.registraionForm.value.firstname + "&lastname=" + this.registraionForm.value.lastname + "&username=" + this.registraionForm.value.username + "&password=" + this.registraionForm.value.password;
    this.http.post(ApiRegistrationURL, null).subscribe((dataFromAPI) => {
      console.log(dataFromAPI);
    })
  }
}
