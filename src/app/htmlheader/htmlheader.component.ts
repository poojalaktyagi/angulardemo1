import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-htmlheader',
  templateUrl: './htmlheader.component.html',
  styleUrls: ['./htmlheader.component.css']
})
export class HTMLHeaderComponent implements OnInit {

  public IfLoggedIn = false;
  public Router = null;
  constructor(private cookieService: CookieService, private router: Router) { 
    this.Router = router;
    this.IfLoggedIn = false;
    let jwtoken = this.cookieService.get('jwtoken');
    //console.warn(this.router.url);
    if (jwtoken === undefined || jwtoken === "") {
      if (this.router.url != "/" && this.router.url != "/login" && this.router.url != "/registartion")
        window.location.href = ('/login');
      return;
    }
    this.IfLoggedIn = true;}

  ngOnInit(): void {
  }
  logout() {
    this.cookieService.delete('jwtoken');
    window.location.href = ('/login');
  }
}
