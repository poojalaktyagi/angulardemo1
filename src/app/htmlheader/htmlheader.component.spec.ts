import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HTMLHeaderComponent } from './htmlheader.component';

describe('HTMLHeaderComponent', () => {
  let component: HTMLHeaderComponent;
  let fixture: ComponentFixture<HTMLHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HTMLHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HTMLHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
