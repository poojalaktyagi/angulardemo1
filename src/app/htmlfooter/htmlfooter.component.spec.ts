import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HTMLFooterComponent } from './htmlfooter.component';

describe('HTMLFooterComponent', () => {
  let component: HTMLFooterComponent;
  let fixture: ComponentFixture<HTMLFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HTMLFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HTMLFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
