import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { CookieService } from 'ngx-cookie-service'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { PersonDashboardComponent } from './person-dashboard/person-dashboard.component';
import { HTMLHeaderComponent } from './htmlheader/htmlheader.component';
import { HTMLFooterComponent } from './htmlfooter/htmlfooter.component';
import { PersonDataModifierModalComponent } from './person-data-modifier-modal/person-data-modifier-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    PersonDashboardComponent,
    HTMLHeaderComponent,
    HTMLFooterComponent,
    PersonDataModifierModalComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
