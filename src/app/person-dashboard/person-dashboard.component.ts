import { Component, OnInit, Output, ElementRef, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { PersonDataModifierModalComponent } from '../person-data-modifier-modal/person-data-modifier-modal.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-person-dashboard',
  templateUrl: './person-dashboard.component.html',
  styleUrls: ['./person-dashboard.component.css']
})
export class PersonDashboardComponent implements OnInit {
  public AllPersons: Observable<any>[] = null;
  @ViewChild('AppPersonDataModifierModal') appPersonDataModifierModal: PersonDataModifierModalComponent;

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  ngOnInit(): void {
    this.updateUI();
  }

  ngAfterViewInit() {
    this.appPersonDataModifierModal.PersonDashboard = this;
  }

  DeletePerson(PersonId) {
    this.http.delete("/api/deleteperson?personid=" + PersonId, { withCredentials: true }).subscribe((dataFromApi) => {
      if (typeof dataFromApi === "string" && (dataFromApi.indexOf("error:") >= 0 || dataFromApi == "jwt must be provided")) {
        console.warn(dataFromApi);
      }
      else {
        this.updateUI();
      }
    });
  }

  open(CrrntPerson) {
    this.appPersonDataModifierModal.personModifierModalTrigger.nativeElement.click();
    this.appPersonDataModifierModal.bindCrrntPerson(CrrntPerson);
  }

  updateUI() {
    this.http.get("/api/getperson", { withCredentials: true }).subscribe((dataFromApi) => {
      //debugger;
      //console.warn("UI Updating");
      if (dataFromApi === undefined || (dataFromApi !== undefined && dataFromApi["status"] === "error")) {
        this.cookieService.delete('jwtoken');
        window.location.href = ('/login');
        return;
      }
      this.AllPersons = dataFromApi as Observable<any>[];
    })
  }

  TrackBy(index: number, item: any) {
    return item.PersonId;

  }

  HandleNull(SomeValue, DefaultValue)
  {
    if(SomeValue === undefined || SomeValue == null|| SomeValue.toString() == 'null')
      return DefaultValue;

    return SomeValue;
  }
}
